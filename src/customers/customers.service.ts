import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) { }

  create(CreateCustomerDto: CreateCustomerDto) {
    return this.customerRepository.save(CreateCustomerDto);
  }

  findAll() {
    return this.customerRepository.find({});
  }

  async findOne(id: number) {
    const customer = await this.customerRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customerRepository.findOneBy({ id: id });
    const updatedCustomer = { ...customer, ...updateCustomerDto };
    return this.customerRepository.save(updatedCustomer);
  }

  async remove(id: number) {
    const customer = await this.customerRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customerRepository.softRemove(customer);
  }
}
