import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { OrderItem } from './entities/order-item';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
  ) { }

  async create(createOrderDto: CreateOrderDto) {
    const customer = await this.customerRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.orderRepository.save(order);

    const orderItems: OrderItem[] = await Promise.all(
      createOrderDto.orderItems.map(async (od) => {
        const orderItem = new OrderItem();
        orderItem.amount = od.amount;
        orderItem.product = await this.productRepository.findOneBy({
          id: od.productId,
        });

        orderItem.name = orderItem.product.name;
        orderItem.price = orderItem.product.price;
        orderItem.total = orderItem.price * orderItem.amount;
        orderItem.order = order; //อ้างอิงกลับ
        return orderItem;
      }),
    );

    for (const orderItem of orderItems) {
      this.orderItemRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }

    await this.orderRepository.save(order);
    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.orderRepository.find({ relations: ['customer', 'orderItems'] });
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItem'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    return this.orderRepository.softDelete(order);
  }
}
